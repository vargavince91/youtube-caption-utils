# YouTube Caption Utils

> Short Python utility scripts interacting with YouTube Data API to fetch and analyze captions.

We use [Google API Python client library](https://developers.google.com/api-client-library/python/). You can browse the source code at [`github.com/google/google-api-python-client`](https://github.com/google/google-api-python-client). Find examples at [`github.com/youtube/api-samples`](https://github.com/youtube/api-samples/tree/master/python)

## Google APIs

**Please make sure you read and understand the Google's API's Terms of Service.**

**Keep your keys/tokens private.**

### Create OAuth 2.0 Client ID

## Development

1. Make sure [Python](https://www.python.org/downloads/) is installed
    ```
    $ python --version && pip --version
    ```
2. Create and activate virtual environment
    ```
    $ python -m venv .
    $ source bin/activate
    ```
3. Install required packages
    ```
    $ pip install -r dev-requirements.txt
    $ pip install -r requirements.txt
    ```